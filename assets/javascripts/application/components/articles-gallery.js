/* Source: https://github.com/kenwheeler/slick/
   ========================================================================== */
const initGallery = () => {
  $('.js-ag-carousel').slick({
    infinite: false,
    speed: 300,
    slidesToShow: 2,
    slidesToScroll: 1,
    nextArrow: '.js-ag-next',
    prevArrow: '.js-ag-before',
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          infinite: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1
        }
      }
      /**
      * You can unslick at a given breakpoint now by adding:
      * settings: "unslick"
      * instead of a settings object
      */
    ]
  })
}

$(function () {
  initGallery()
})
