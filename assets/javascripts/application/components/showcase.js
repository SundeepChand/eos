let $eleDisplayTemplate
$(function () {
  Prism.highlightAll() // eslint-disable-line no-undef
  const component = $('.js-element-id').attr('name')
  // Prepare li to show component showcase radio buttons
  $eleDisplayTemplate = $('.js-showcase-current').clone(true)
  $('.js-showcase-current').remove()
  getComponentType(component)
})

const renderComponentShowcase = (componentShowcases, component) => {
  const componentCollection = componentShowcases
    .filter((ele) => ele.component === `${component}`)
    .map((ele) => ele.states)
  for (let i = 0; i < componentCollection[0].length; i++) {
    const compShowcase = componentCollection[0][i].status

    const newEleDisplay = $eleDisplayTemplate.clone(true)

    // Make first letter uppercase and add space between words
    const labelText =
      compShowcase.substr(0, 1).toUpperCase() +
      compShowcase.substr(1).split('-').join(' ')

    // Add id value to input field
    $(newEleDisplay)
      .find('.js-element-id')
      .attr({ id: `${compShowcase}-showcase`, value: `${compShowcase}` })
    $(newEleDisplay)
      .find('.js-element-label')
      .attr('for', `${compShowcase}-showcase`)
      .text(`${labelText}`)
    $('.js-showcase-list').append(newEleDisplay)
  }

  // Add checked attibute to default showcase on page load
  $('#default-showcase').prop('checked', true)

  // Add default text to How to use and code container on page load
  updateComponentShowcase(componentCollection[0], 'default')

  // Change text in How to use and code container on click
  $('.js-showcase-current input').click(function () {
    const component = $(this).attr('value')

    // Update components info
    updateComponentShowcase(componentCollection[0], component)
  })
}

const updateComponentShowcase = (componentCollection, component) => {
  const componentShowcaseData = componentCollection.filter(
    (ele) => ele.status === `${component}`
  )

  if (componentShowcaseData[0].how_to_use !== '') {
    $('.js-how-to-use').html(componentShowcaseData[0].how_to_use)
    $('.js-showcase-how-to').show()
    $('.js-how-to-use').show()
  } else {
    $('.js-showcase-how-to').hide()
    $('.js-how-to-use').hide()
  }

  let newCode = ''
  for (let i = 0; i < componentShowcaseData[0].code.length; i++) {
    newCode += `${componentShowcaseData[0].code[i]}
`
  }
  $('.js-example-inner-box').html(newCode)
  $('.js-snippet-code code').text(newCode)
  Prism.highlightAll() // eslint-disable-line no-undef
  $('*[data-toggle~="tooltip"]').tooltip('update')
}

function getComponentType (component) {
  getComponentService((result) => { // eslint-disable-line no-undef
    // eslint-disable-line no-undef
    const componentShowcases = result

    renderComponentShowcase(componentShowcases, component)
  })
}
