$(function () {
  /* Select all the code blocks */
  const elements = $('.js-smspecs-style')

  for (let index = 0; index < elements.length; index++) {
    const element = elements[index]

    smartSpecs({
      target: element,
      focus: false
    })
  }
})

function smartSpecs (args) {
  const { target } = args

  /* TODO: Extra props should be part of the .js-smspecs-code data-attribute so we spread it to the default list */
  const desiredProps = ['font-family', 'color', 'background-color', 'font-size', 'font-weight', 'margin-top', 'margin-bottom', 'margin-left', 'margin-right', 'padding-top', 'padding-bottom', 'padding-left', 'padding-right', 'border-top-width', 'border-bottom-width', 'border-left-width', 'border-right-width']

  desiredProps.forEach(value => {
    /* Get each computed style */
    /* Colors are read as RBG values, there are plenty of functions for conventions https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb  */
    const element = $(target).find('.js-smspecs-code').children()[0]
    const theCSSprop = window.getComputedStyle(element, null).getPropertyValue(value)

    /* Fill with margins properties */
    if (theCSSprop.length > 0 && value.includes('margin')) {
      switch (value) {
        case 'margin-top':
          return $(target).find('.js-smspecs-m-top').text(theCSSprop)
        case 'margin-right':
          return $(target).find('.js-smspecs-m-right').text(theCSSprop)
        case 'margin-bottom':
          return $(target).find('.js-smspecs-m-bottom').text(theCSSprop)
        case 'margin-left':
          return $(target).find('.js-smspecs-m-left').text(theCSSprop)
        default:
          return value
      }
    }

    if (theCSSprop.length > 0 && value.includes('padding')) {
      switch (value) {
        case 'padding-top':
          return $(target).find('.js-smspecs-p-top').text(theCSSprop)
        case 'padding-right':
          return $(target).find('.js-smspecs-p-right').text(theCSSprop)
        case 'padding-bottom':
          return $(target).find('.js-smspecs-p-bottom').text(theCSSprop)
        case 'padding-left':
          return $(target).find('.js-smspecs-p-left').text(theCSSprop)
        default:
          return value
      }
    }

    /* Fill with padding properties */
    if (theCSSprop.length > 0 && value.includes('border')) {
      switch (value) {
        case 'border-top-width':
          return $(target).find('.js-smspecs-b-top').text(theCSSprop)
        case 'border-right-width':
          return $(target).find('.js-smspecs-b-right').text(theCSSprop)
        case 'border-bottom-width':
          return $(target).find('.js-smspecs-b-bottom').text(theCSSprop)
        case 'border-left-width':
          return $(target).find('.js-smspecs-b-left').text(theCSSprop)
        default:
          return value
      }
    }

    /* Fill the rest of the properties */
    if (theCSSprop.length > 0) {
      return $(target).find('.js-smspecs-props-list').append(`<li><code>${value}: ${theCSSprop}</code></li>`)
    }
  })
}
