/* Prepare local variables that will be reused in different methods */
let $comingNextContainer, _comingNextItemTemplate

$(function () {
  // Prepare coming next containers
  $comingNextContainer = $('.js-coming-next-list')
  _comingNextItemTemplate = $('.js-coming-next-item').clone(true)

  $('.js-coming-next-item').remove()

  // Initiate the full collection of coming-next collection
  if ($('.js-comingNextController').length) {
    comingNext()
  }
  checkAlertCookie()

  // Set alert cookies
  $('.js-set-alert-cookies').on('click', () => {
    comingNextAlertCookie('coming-next-acceptance', 'true', 40)
  })
})

const comingNextAlertCookie = (name, value, days) => {
  let expires
  if (days) {
    const date = new Date()
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000))
    expires = `; expires=${date.toGMTString()}`
  } else {
    expires = ''
  }
  Cookies.set(name, value, { expire: expires }) // eslint-disable-line no-undef
}

const comingNext = () => {
  getComingNext(_data => { // eslint-disable-line no-undef
    const comingNextData = _data.data.comingnexts

    for (let i = 0; i < comingNextData.length; i++) {
      const newComingNextItem = _comingNextItemTemplate.clone(true)

      // Add design specs info
      $(newComingNextItem).find('.js-coming-next-title').text(comingNextData[i].title)
      $(newComingNextItem).find('.js-coming-next-description').text(comingNextData[i].description)
      $(newComingNextItem).find('.js-coming-next-image').attr('src', comingNextData[i].image)
      $($comingNextContainer).append(newComingNextItem)
    }
  })
}

const checkAlertCookie = () => {
  /* eslint-disable no-undef */
  const comingNextAcceptance = Cookies.get('coming-next-acceptance')
  if (comingNextAcceptance === undefined) {
    $('.js-component-alert').removeClass('alert-hidden')
  }
}
