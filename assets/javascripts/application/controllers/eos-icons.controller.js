/* ==========================================================================
  Icons version and links
  ========================================================================== */

/* Prepare local variables that will be reused in different methods */
let _iconsContainerList, $iconsContainer, $newIconsContainerList, $categoryContainer, $iconDisplayTemplate, $tagTemplate, $animatedIconDisplayTemplate
// Array of animated icons type need to fix this in json
const animatedType = ['animate', 'animated']

$(function () {
  // Prepare icons containers
  $iconsContainer = $('.js-icons-container')
  $('.js-eos-icons-notification').hide()
  _iconsContainerList = $('.js-eos-icons-list').clone(true)
  $('.js-eos-icons-list').remove()
  $categoryContainer = $('.js-icons-category-name').clone(true)
  $('.js-icons-category-name').remove()

  if ($('.js-eosIconsSet').length) {
    // Send an empty search to return the full collection of icons
    getIconsCollection('')
  }

  // Tag span clone
  $tagTemplate = $('.js-eos-icons-tag').clone(true)

  /* Better search response and API call
  ========================================================================== */
  let typingTimer
  const doneTypingInterval = 800

  /* on keyup, start the countdown */
  $('.js-eos-icon-filter').on('keyup', function () {
    $('.js-eos-icons-notification').hide()
    $('.js-search-icon').text('close')
    clearTimeout(typingTimer)
    typingTimer = setTimeout(doneTyping, doneTypingInterval)
  })

  /* on keydown, clear the countdown */
  $('.js-eos-icon-filter').on('keydown', function () {
    clearTimeout(typingTimer)
  })

  /* once typing is done */
  function doneTyping () {
    const query = $('.js-eos-icon-filter').val()
    onSearch(query)
  }

  $('.js-search-icon').on('click', function () {
    $('.js-eos-icon-filter').val('')
    $('.js-search-icon').text('search')
    $('.js-eos-icons-notification').hide()
    onSearch('')
  })
})

const onSearch = (q = '') => {
  $('.js-eos-icons-list').remove()
  $('.js-icon-display').remove()
  $('.js-animated-icon-display').remove()
  $('.js-icons-category-name').remove()
  getIconsCollection(q)
}

const renderIcons = (collection) => {
  const uniqueAges = [...new Set(collection.map(obj => Array.isArray(obj.category) ? obj.category[0] : obj.category))]

  for (let j = 0; j < uniqueAges.length; j++) {
    const categoryName = uniqueAges[j]
    const categoryCollection = collection.filter(cate => Array.isArray(cate.category) ? cate.category[0] === `${categoryName}` : cate.category === `${categoryName}`)

    const newCategoryContainer = $categoryContainer.clone(true)

    const newCategoryName = categoryName.substr(0, 1).toUpperCase() + categoryName.substr(1)
    $(newCategoryContainer).text(newCategoryName)
    $newIconsContainerList = _iconsContainerList.clone(true)

    // Prepare animated and static icon containers
    $animatedIconDisplayTemplate = $($newIconsContainerList).find('.js-animated-icon-display')
    $iconDisplayTemplate = $($newIconsContainerList).find('.js-icon-display')
    $($newIconsContainerList).find('.js-animated-icon-display').remove()
    $($newIconsContainerList).find('.js-icon-display').remove()

    for (let i = 0; i < categoryCollection.length; i++) {
      const iconName = categoryCollection[i].name
      const animatedType = ['animate', 'animated']

      const newIconDisplay = animatedType.indexOf(categoryCollection[i].type) < 0 ? $iconDisplayTemplate.clone(true) : $animatedIconDisplayTemplate.clone(true)
      $(newIconDisplay).attr('id', `${iconName}`)

      // Add icon name
      $(newIconDisplay).find('.js-animated-eos-icons').attr('src', `/images/animated-icons/${iconName}-gray.svg`)
      $(newIconDisplay).find('.js-icon-name').text(iconName)
      $($newIconsContainerList).append(newIconDisplay)

      /* Attach the click event on creation */
      $(newIconDisplay).on('click', function () {
        toggleEosIconInPanel(iconName)
      })
    }

    $($iconsContainer).append(newCategoryContainer)
    $($iconsContainer).append($newIconsContainerList)
  }
}

const toggleEosIconInPanel = (iconName) => {
  $('.js-eos-icons-name').text(iconName)

  /* Get information for the Do and Dont and the Tags */
  getMoreInfoFromIconService(iconName, function (data) { // eslint-disable-line no-undef
    const notFoundMsg = 'No information available'
    const type = data ? data.type || null : null
    const textDo = data ? data.do || notFoundMsg : notFoundMsg
    const textDont = data ? data.dont || notFoundMsg : notFoundMsg
    const tags = data.tags !== undefined ? data.tags : ' '

    addIconData(textDo, textDont, tags)

    if (animatedType.indexOf(type) < 0) {
      $('.js-eos-icons-code-example').removeClass('d-none')
      $('.js-eos-animation-icons-code-example').addClass('d-none')
    } else {
      $('.js-eos-icons-code-example').addClass('d-none')
      $('.js-eos-animation-icons-code-example').removeClass('d-none')
      addAnimatedIconSpecificData(iconName) // eslint-disable-line no-undef
    }
  })
}

const addIconData = (textDo, textDont, tags) => {
  $('.js-eos-icons-do').html(jQuery.parseHTML(textDo))
  $('.js-eos-icons-dont').html(jQuery.parseHTML(textDont))

  $('.js-eos-icons-tag').remove()
  for (let i = 0; i < tags.length; i++) {
    const _tagTemplate = $tagTemplate.clone(true)
    $(_tagTemplate).text(tags[i])
    $('.js-eos-icons-tags-list').append(_tagTemplate)
  }
}

/* Declared functions
========================================================================== */
/*
 In order to access this function from Unit Testing, we need to make it a declared function
 dont change this to arrow function or unit test will break
*/

// don't use the arrow function here
function getIconsCollection (q) {
  getIconsService(q, function (searchResult) { // eslint-disable-line no-undef
    const iconsMerged = searchResult
    renderIcons(iconsMerged)
  })
}
