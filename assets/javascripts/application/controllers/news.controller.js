$(function () {
  /* Check page classname to decide if we pull or not the data. */
  if ($('.js-newsController').length) {
    dashboardNews()
  }
})

const dashboardNews = () => {
  getNews(_data => { // eslint-disable-line no-undef
    /* Check for data and show news section */
    const newsLength = _data.data.news.length
    const newsArray = _data.data.news

    if (newsLength > 0) {
      $('.js-news-loading-wrap').fadeOut().remove()
      $('.js-news-tutorials-carousel').removeClass('hide')
    }

    const sectionTarget = $('.news-tutorials-carousel')

    /* Copy of the template for videos and news */
    const $video = $('.js-video-container').clone(true)
    const $article = $('.js-article-container').clone(true)

    /* Remove blocks from landing after saving a copy in memory */
    $('.js-video-container').remove()
    $('.js-article-container').remove()

    /* We send the data, templates and target to createNewsBox */
    createNewsBoxes(newsArray, sectionTarget, $video, $article)
    /* Get video only when the box is clicked */
    videoLoadOnClick() // eslint-disable-line no-undef
    /* Init carousel after appending all the data */
    initCarousel()
  })
}

/* Create news boxes for videos and articles */
const createNewsBoxes = (data, target, video, article) => {
  data.map(element => element.enabled ? addBoxToPage(target, element.selectType === 'video' ? video : article, element) : null)
}

/* Function that generate the box for videos or articles */
const addBoxToPage = (target, template, data) => {
  const _template = template.clone(true)
  const articleRoute = symbolsParser(data.title)

  const { title, subtitle, videourl, date } = data
  const regEx = /[0-9]*-[0-9]*-[0-9]*/gm
  const useDate = date.match(regEx)

  /* Set title, thumbnail and url */
  _template.find('.js-card-date').text(useDate)
  _template.find('h4').text(title)
  _template.find('p').text(subtitle)
  data.selectType === 'video' ? _template.attr('data-video-url', videourl) : _template.attr('href', `news/${articleRoute}`)

  return target.append(_template)
}

/* Converte the new title so we can use as route */
const symbolsParser = (str) => {
  const regEx = /(\w+)/g
  return str.toLowerCase().match(regEx).join('-')
}

/* Init carousel for home */
const initCarousel = () => {
  /* Init the carousel */
  $('.js-news-tutorials-carousel').slick({
    infinite: false,
    speed: 300,
    slidesToShow: 2,
    slidesToScroll: 1,
    prevArrow: '.js-news-tutorials-before',
    nextArrow: '.js-news-tutorials-next',
    responsive: [
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  })
}
