const axios = require('axios')

const strapiGraphql = async query => {

  if (!process.env.EOS_RUN_PROJECT_AS) return console.error('⚠️  No process.env.EOS_RUN_PROJECT_AS was found. Please check your .env file before continuing')
  /* We set an config object to map the values for production and development bases on the value of EOS_RUN_PROJECT_AS */
  const config = {
    dev: {
      url: process.env.EOS_STRAPI_SERVER_DEV,
      username: process.env.EOS_STRAPI_USERNAME_DEV,
      password: process.env.EOS_STRAPI_PASSWORD_DEV
    },
    prod: {
      url: process.env.EOS_STRAPI_SERVER_PROD,
      username: process.env.EOS_STRAPI_USERNAME_PROD,
      password: process.env.EOS_STRAPI_PASSWORD_PROD
    }
  }

  try {
    const { url } = config[process.env.EOS_RUN_PROJECT_AS]
    const { data } = await axios.get(`${url}/graphql?query={${query}}`)

    return data
  } catch (error) {
    console.log('strapiGraphql(): ', error);
  }
}

module.exports = {
  strapiGraphql,
}
