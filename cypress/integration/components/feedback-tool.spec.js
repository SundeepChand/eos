describe("feedback-tool", () => {
  before(() => {
    cy.setCookie('annoucement', '{%22wasShown%22:true%2C%22campaing%22:%22SUSE%20rebrand%22}')
    cy.server();
    cy.route('POST', '**/feedback', 'fixture:feedback-tool.json').as('sendFeedback')

    cy.visit('/')
    cy.get('.feedback-tool-button').click()
  })

  it('should not be able to submit if msg length is less that minium', () => {
    cy.get("textarea[name ='feedback-tool-message']").type('Hey')
    cy.get('.js-feedback-tool-send').should('have.class', 'disabled')
  })

  it('should be able to submit if length is longer than min required', () => {
    cy.get("textarea[name ='feedback-tool-message']").type('Hey, this is a longer message so we can test the min message width')
    cy.get('.js-feedback-tool-screenshoot-trigger').click()
    cy.get('.js-feedback-tool-send').should('not.have.class', 'disabled')
  })
});
