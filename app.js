const express = require('express')
const path = require('path')
const favicon = require('serve-favicon')
const logger = require('morgan')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const robots = require('express-robots-txt')
const { strapiMiddleware } = require('./modules/strapi-middleware')
const { authMiddleware } = require('./modules/auth-middleware')
const cron = require('node-cron')
const axios = require('axios')
const dotEnv = require('dotenv')
const rateLimit = require('express-rate-limit')
const session = require('express-session')

/* =============================
  Routes files declarations
============================= */
// index pages
const icons = require('./routes/icons/index')
const home = require('./routes/home/index')
const brandColors = require('./routes/colors/index')
const dashboardWriting = require('./routes/writing/index')
const cookiesPolicy = require('./routes/shared/index')
const changeLog = require('./routes/changelog/index')
const buttons = require('./routes/buttons/index')
const typography = require('./routes/typography/index')
const alerts = require('./routes/alerts/index')
const patterns = require('./routes/patterns/index')
const layout = require('./routes/layout/index')
const links = require('./routes/links/index')
const tooltips = require('./routes/tooltips/index')
const auth = require('./routes/auth/index')
const select = require('./routes/form-elements/select/index')
const checkbox = require('./routes/form-elements/checkbox/index')
const signin = require('./routes/templates/sign-in/index')
const comingNext = require('./routes/coming-next/index')
const textArea = require('./routes/form-elements/text-area/index')
const address = require('./routes/form-elements/address/index')
const radioButtons = require('./routes/form-elements/radio-buttons/index')
const toggle = require('./routes/form-elements/toggle/index')
const tables = require('./routes/tables/index')
const badges = require('./routes/badges/index')
const cards = require('./routes/cards/index')
const pagination = require('./routes/pagination/index')
const dropdown = require('./routes/dropdown/index')

// submenu pages
const colorsUsage = require('./routes/colors/usage')
const eosIconsSet = require('./routes/icons/eos-icons-set')
const commonIcons = require('./routes/icons/commonly-used')
const logoIconsSet = require('./routes/icons/logo-icons-set')
const arrowsUsage = require('./routes/icons/arrows-usage')
const uxWriting = require('./routes/writing/ux-writing')
const brandVoice = require('./routes/writing/brand-voice')
const brandTone = require('./routes/writing/brand-tone')
const writingAcronyms = require('./routes/writing/acronyms')
const conventionsAndRules = require('./routes/writing/conventions-and-rules')
const iconsUx = require('./routes/icons/icon-ux')
const iconButtons = require('./routes/buttons/icon-buttons')
const buttonsSizing = require('./routes/buttons/sizing')
const buttonsPositioning = require('./routes/buttons/positioning')
const buttonsHowToUse = require('./routes/buttons/usage')
const buttonGroup = require('./routes/buttons/button-group')
const alertsGlobal = require('./routes/alerts/global')
const alertsSection = require('./routes/alerts/section')
const alertsInline = require('./routes/alerts/inline')
const alertsUsage = require('./routes/alerts/usage')
const animatedIconsPattern = require('./routes/patterns/loading')
const spacingPattern = require('./routes/patterns/spacing')
const commonActions = require('./routes/patterns/common-actions')
const dateTime = require('./routes/patterns/date-time')
const documentationInstallingNpm = require('./routes/documentation/installing-npm')
const documentationDesignSpecs = require('./routes/documentation/design-specs')
const documentationDesignKit = require('./routes/documentation/design-kit')
const layoutMainMenu = require('./routes/layout/main-menu')
const smartTooltips = require('./routes/tooltips/smart-tooltip')
const layoutSubmenu = require('./routes/layout/submenu')
const layoutFooter = require('./routes/layout/footer')
const textInputFields = require('./routes/form-elements/input-fields/index')
const passwordInputFields = require('./routes/form-elements/input-fields/password')
const rangeInputFields = require('./routes/form-elements/input-fields/range')
const fileInputFields = require('./routes/form-elements/input-fields/file')
const signinSignup = require('./routes/templates/sign-in/sign-up')
const signinPassword = require('./routes/templates/sign-in/password-recovery')
const signinAlerts = require('./routes/templates/sign-in/alerts-in-sign-in')
const multipleSelection = require('./routes/form-elements/select/multiple-selection')
const addressDisplaying = require('./routes/form-elements/address/displaying-addresses')
const errorPage = require('./routes/templates/error-page/index')
const darkTables = require('./routes/tables/dark')

// news
const news = require('./routes/news/index')
// Internal pages
const designSpecs = require('./routes/internal/design-specs')

// API
const iconsAPI = require('./routes/api/icons/index')
const gitlabAPI = require('./routes/api/gitlab/index')
const feedbackTool = require('./routes/api/feedback-tool/index')
const logoIcons = require('./routes/api/logo-icons/index')
const strapiQuery = require('./routes/api/strapi/index')
const mongodbRoute = require('./routes/api/mongodb/index')
const authRoute = require('./routes/api/auth/index')

const app = express()

/* Rate limiter for specific endpoints
  ========================================================================== */
const limiter = rateLimit({
  windowMs: 480 * 60 * 1000, // Timeout in milliseconds (8h)
  max: 3 // Number of requests allowed per user
})

app.use('/api/mongodb/', limiter)

/* TODO: Add the secret as env */
app.use(
  session({
    secret: 'weAreEosMan',
    name: 'session',
    resave: false,
    saveUninitialized: false,
    cookie: {
      maxAge: 2592000000 // time in MS (30 days)
    }
  })
)

/* =============================
  Redirects for SEO
============================= */
app.get('/colors/how-to/', (req, res, next) => {
  res.redirect(301, '/colors/usage/')
})

app.get('/icons/when-to-use-them/', (req, res, next) => {
  res.redirect(301, '/icons/eos-icons-set/')
})

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

// use env variables
app.set(dotEnv.config())

app.use(favicon(path.join(__dirname, 'assets/images', 'favicon.png')))
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'assets')))
app.use(express.static(path.join(__dirname, 'vendors')))
app.use(strapiMiddleware)
app.use(authMiddleware)

/* Set public folder for sitemaps, pdf etc.. */
app.use(express.static(path.join(__dirname, 'public')))
/* Set robots.txt with express middleware, for the sitemap, place it inside /public folder */
if (
  process.env.NODE_ENV === 'development' ||
  process.env.NODE_ENV === 'staging' ||
  process.env.NODE_ENV === undefined
) {
  /* Disallow bots for development/staging */
  app.use(robots({ UserAgent: '*', Disallow: '/' }))
}
if (process.env.NODE_ENV === 'production') {
  /* Allow bots for production */
  app.use(
    robots({
      UserAgent: '*',
      Disallow: ['/cookies-policy'],
      Sitemap: 'https://suse.eosdesignsystem.com/sitemap.xml'
    })
  )
}

/* =============================
  Routes definitions
============================= */
// index pages routes
app.use('/', home)
app.use('/icons', icons)
app.use('/colors', brandColors)
app.use('/writing', dashboardWriting)
app.use('/cookies-policy', cookiesPolicy)
app.use('/changelog', changeLog)
app.use('/buttons', buttons)
app.use('/typography', typography)
app.use('/alerts', alerts)
app.use('/patterns', patterns)
app.use('/layout', layout)
app.use('/links', links)
app.use('/tooltips', tooltips)
app.use('/auth', auth)
app.use('/select', select)
app.use('/templates/sign-in', signin)
app.use('/coming-next', comingNext)
app.use('/tables', tables)
app.use('/badges', badges)
app.use('/cards', cards)
app.use('/pagination', pagination)
app.use('/dropdown', dropdown)

// submenu routes
app.use('/colors/usage', colorsUsage)
app.use('/icons/commonly-used', commonIcons)
app.use('/icons/eos-icons-set', eosIconsSet)
app.use('/icons/icon-ux', iconsUx)
app.use('/icons/arrows-usage', arrowsUsage)
app.use('/icons/logo-icons-set', logoIconsSet)
app.use('/writing/ux-writing', uxWriting)
app.use('/writing/brand-voice', brandVoice)
app.use('/writing/brand-tone', brandTone)
app.use('/writing/acronyms', writingAcronyms)
app.use('/writing/conventions-and-rules', conventionsAndRules)
app.use('/internal/design-specs', designSpecs)
app.use('/buttons/icon-buttons', iconButtons)
app.use('/buttons/sizing', buttonsSizing)
app.use('/buttons/positioning', buttonsPositioning)
app.use('/buttons/usage', buttonsHowToUse)
app.use('/buttons/button-group', buttonGroup)
app.use('/alerts/global', alertsGlobal)
app.use('/alerts/section', alertsSection)
app.use('/alerts/inline', alertsInline)
app.use('/alerts/usage', alertsUsage)
app.use('/patterns/loading', animatedIconsPattern)
app.use('/patterns/spacing', spacingPattern)
app.use('/patterns/common-actions', commonActions)
app.use('/patterns/date-time', dateTime)
app.use('/layout/main-menu', layoutMainMenu)
app.use('/layout/submenu', layoutSubmenu)
app.use('/layout/footer', layoutFooter)
app.use('/tooltips/smart-tooltips', smartTooltips)
app.use('/form-elements/checkbox', checkbox)
app.use('/form-elements/input-fields', textInputFields)
app.use('/form-elements/input-fields/password', passwordInputFields)
app.use('/form-elements/input-fields/range', rangeInputFields)
app.use('/form-elements/input-fields/file', fileInputFields)
app.use('/templates/sign-in/sign-up', signinSignup)
app.use('/templates/sign-in/password-recovery', signinPassword)
app.use('/templates/sign-in/alerts-in-sign-in', signinAlerts)
app.use('/form-elements/select', select)
app.use('/form-elements/select/multiple-selection', multipleSelection)
app.use('/form-elements/text-area', textArea)
app.use('/form-elements/address', address)
app.use('/form-elements/address/displaying-addresses', addressDisplaying)
app.use('/form-elements/radio-buttons', radioButtons)
app.use('/form-elements/toggle', toggle)
app.use('/templates/error-page/', errorPage)
app.use('/tables/dark/', darkTables)

// Documentation
app.use('/installing-npm', documentationInstallingNpm)
app.use('/design-specs', documentationDesignSpecs)
app.use('/design-kit', documentationDesignKit)
// news routes
app.use('/news', news)
// API
app.use('/api/icons', iconsAPI)
app.use('/api/gitlab', gitlabAPI)
app.use('/api/gitlab/repository', gitlabAPI)
app.use('/api/feedback', feedbackTool)
app.use('/api/logo-icons', logoIcons)
app.use('/api/strapi', strapiQuery)
app.use('/api/mongodb', mongodbRoute)
app.use('/api/auth', authRoute)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  res.status(404).render('error/404/index')
})

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500)
    res.render('error', {
      message: err.message,
      error: err
    })
  })
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500)
  res.render('error', {
    message: err.message,
    error: {}
  })
})

/* Run a cronjob every Sunday at 00:00 */
cron.schedule('0 0 * * 0', async () => {
  return await axios.get(
    `http://127.0.0.1:${process.env.PORT || 3000}/api/mongodb`
  )
})

module.exports = app
